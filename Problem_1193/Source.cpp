
#include <bitset>
#include <string>
#include <iostream>

using std::string;
using std::cin;
using std::cout;
using std::endl;

int main() {
  int N, K, Q;
  int(*qs)[2];

  cin >> N >> K >> Q;

  string word;
  cin >> word;

  qs = new int[Q][2];
  for (int i = 0; i < Q; i++) {
    cin >> qs[i][0] >> qs[i][1];
  }

  int *reps = new int[N] { 1 };
  for (int i = 1; i < N; i++) {
    if (word[i] == word[i - 1])
      reps[i] = reps[i - 1] + 1;
    else
      reps[i] = 1;
  }
  int result = 0;
  for (int i = 0; i < N; i++) {
    int sub = 0;
    for (int j = i; j < N; j++) {
      if (reps[j] < K)
        sub++;
      else
        break;
    }
    result += sub;
  }

  cout << result << endl;

  return 0;
}